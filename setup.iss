#define MyAppName "Debug Helper"
#define MyAppVersion "0.0.0"
#define MyAppPublisher "Christoph Jüngling"
#define MyAppURL "https://gitlab.com/users/juengling/debughelper"
#define MyAppExeName "debughelper.exe"

[Setup]
AppId=cp4d
AppName={#MyAppName}
AppVersion={#MyAppVersion}
VersionInfoVersion={#MyAppVersion}
VersionInfoTextVersion={#MyAppVersion}
VersionInfoCompany={#MyAppPublisher}
VersionInfoCopyright={#MyAppPublisher}
VersionInfoProductName={#MyAppName}
VersionInfoProductVersion={#MyAppVersion}
AppVerName={#MyAppName} v{#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}/issues
AppUpdatesURL={#MyAppURL}/releases
DisableWelcomePage=False
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
OutputDir=.
OutputBaseFilename=debughelper-setup
Compression=lzma
SolidCompression=yes
WizardImageFile=compiler:WizModernImage-IS.bmp
WizardSmallImageFile=compiler:WizModernSmallImage-IS.bmp
ShowLanguageDialog=auto
ChangesEnvironment=True
SignTool=cjsign
SignedUninstaller=True

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl"; InfoBeforeFile: "Recent Changes EN.rtf"
Name: "de"; MessagesFile: "compiler:Languages\German.isl"; InfoBeforeFile: "Recent Changes DE.rtf"

[Messages]
en.WelcomeLabel2=This will install [name/ver] on your computer.
de.WelcomeLabel2=Dieser Assistent wird jetzt [name/ver] auf Ihrem Computer installieren.

[CustomMessages]
en.store=Set folder as new destination
de.store=Speichere Ordner als zukünftiges Ziel

en.copy=Copy folder to stored destination
de.copy=Kopiere Ordner an das gemerkte Ziel

en.show=Show stored destination
de.show=Zeige das gemerkte Ziel

en.menus=Context menu entries:
de.menus=Kontextmeneinträge:

en.hierarchical=hierarchical
de.hierarchical=hierarchisch

en.flat=flat (choose if hierarchical doesn't work)
de.flat=flach (wähle diese Einstellung, wenn 'hierarchisch' nicht funktioniert)

[Files]
Source: "dist\{#MyAppExeName}"; DestDir: "{app}"; Flags: recursesubdirs
Source: "DebugHelper.reg"; DestDir: "{app}"; Flags: promptifolder comparetimestamp

[Tasks]
Name: "hierarchical"; Description: "{cm:hierarchical}"; GroupDescription: "{cm:menus}"; Flags: exclusive
Name: "flat"; Description: "{cm:flat}"; GroupDescription: "{cm:menus}"; Flags: exclusive unchecked

[Registry]
; Both menu types
Root: "HKCR"; Subkey: "Directory\shell"; ValueType: string; ValueData: "none"; Tasks: flat hierarchical

; Flat menues
Root: "HKCR"; Subkey: "Directory\shell\{#MyAppName}: {cm:copy}"; Flags: uninsdeletekey; Tasks: flat
Root: "HKCR"; Subkey: "Directory\shell\{#MyAppName}: {cm:copy}\command"; ValueType: string; ValueData: "{app}\{#MyAppExeName} --source ""%1"""; Flags: uninsdeletekey; Tasks: flat

Root: "HKCR"; Subkey: "Directory\shell\{#MyAppName}: {cm:store}"; Flags: uninsdeletekey; Tasks: flat
Root: "HKCR"; Subkey: "Directory\shell\{#MyAppName}: {cm:store}\command"; ValueType: string; ValueData: "{app}\{#MyAppExeName} --destination ""%1"""; Flags: uninsdeletekey; Tasks: flat

Root: "HKCR"; Subkey: "Directory\shell\{#MyAppName}: {cm:show}"; Flags: uninsdeletekey; Tasks: flat
Root: "HKCR"; Subkey: "Directory\shell\{#MyAppName}: {cm:show}\command"; ValueType: string; ValueData: "{app}\{#MyAppExeName} --show-destination"; Flags: uninsdeletekey; Tasks: flat

; Hierarchical menues
Root: "HKCR"; Subkey: "Directory\shell\{#MyAppName}"; ValueType: string; ValueName: "MUIVerb"; ValueData: "{#MyAppName}"; Flags: uninsdeletekey; Tasks: hierarchical
Root: "HKCR"; Subkey: "Directory\shell\{#MyAppName}"; ValueType: string; ValueName: "SubCommands"; ValueData: "DebugHelper.Copy;DebugHelper.Set;DebugHelper.Show"; Flags: uninsdeletekey; Tasks: hierarchical

Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\DebugHelper.Copy"; ValueType: string; ValueData: "{cm:copy}"; Flags: uninsdeletekey; Tasks: hierarchical
Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\DebugHelper.Copy\command"; ValueType: string; ValueData: "{app}\{#MyAppExeName} --source ""%1"""; Flags: uninsdeletekey; Tasks: hierarchical

Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\DebugHelper.Set"; ValueType: string; ValueData: "{cm:store}"; Flags: uninsdeletekey; Tasks: hierarchical
Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\DebugHelper.Set\command"; ValueType: string; ValueData: "{app}\{#MyAppExeName} --destination ""%1"""; Flags: uninsdeletekey; Tasks: hierarchical

Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\DebugHelper.Show"; ValueType: string; ValueData: "{cm:show}"; Flags: uninsdeletekey; Tasks: hierarchical
Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\DebugHelper.Show\command"; ValueType: string; ValueData: "{app}\{#MyAppExeName} --show-destination"; Flags: uninsdeletekey; Tasks: hierarchical

[INI]
Filename: "{userappdata}\debughelper.ini"; Section: "Options"; Key: "Language"; String: "{language}"; Flags: createkeyifdoesntexist

'''
Created on 17.02.2016

@author: Christoph Juengling
'''

from argparse import ArgumentParser
from os import walk, path, makedirs
from shutil import copyfile
from zipfile import ZipFile, ZIP_DEFLATED
import ctypes
import logging
import sys
import winreg

from tqdm import tqdm
from win32com.client import Dispatch
from win32con import MB_OK, MB_ICONINFORMATION

__myname__ = 'debughelper'
__myshortname__ = 'dh'
__title__ = 'Debug Helper'
__version__ = '1.1.4'

REG_PATH = 'SOFTWARE\\juengling\\' + __myname__

# Return codes for command line
__return_ok__ = 0
__return_nodestination__ = 1
__return_error__ = 9


def main():
    '''
    Copy a folder to a local resource for debugging purposes,
    zip it and create a link to the source.
    '''
    args = parse_command_line()
    logging.basicConfig(level=getloggingmode(args.verbose))
    logger = logging.getLogger('main')

    if args.version:
        print(__title__ + ' v' + __version__)
        print(main.__doc__.strip())
        print('Use -h/--help for additional information.')
        return __return_ok__

    if args.print:
        print('Stored destination: {}'.format(get_reg('Destination')))
        return __return_ok__

    if args.source:
        if args.destination:
            destination = args.destination
            logger.info('Store destination %s to registry', destination)
            set_reg('Destination', args.destination)
        else:
            destination = get_reg('Destination')
            logger.info('Read destination %s from registry', destination)
            if not destination or destination == '':
                print('Destination not set!')
                return __return_nodestination__

    elif args.destination:
        logger.info('Store destination for later use.')
        set_reg('Destination', args.destination)
        logger.info('Destination stored: %s', args.destination)
        return __return_ok__

    elif args.show_destination:
        destination = get_reg('Destination')
        if destination and destination != '':
            message = 'Stored destination: {}'.format(destination)
        else:
            message = 'No destination stored.'

        logger.info(message)
        print(message)
        msgbox(message)

        return __return_ok__

    else:
        logger.warning(
            'At least source or destination must be specified.'
            ' Use "%s --help" for details.', __myname__)
        return __return_nodestination__

    # Add last part of source path to destination
    lastpart = args.source.split('\\')[-1:][0]
    folder_destination = path.join(destination, lastpart)

    try:
        if args.dry_run:
            logger.info('Retrieving size ...')
            size = get_size(args.source)
            logger.info('Size is %.1f %s', size[0], size[1])
        else:
            recursivecopy(args.source, folder_destination)

        if not args.dry_run:
            zipdir(folder_destination, path.join(
                destination, lastpart + '-Original.zip'))
            createlink(
                path.join(destination, lastpart + '-Quelle'), args.source)

    except Exception as e:
        logger.exception(e)
        return __return_error__

    print('Done.')
    return __return_ok__


def parse_command_line():
    parser = ArgumentParser(
        prog=__myname__, description=main.__doc__)
    parser.add_argument(
        '--version', action='store_true', help='Print version info and exit')
    parser.add_argument('-s', '--source', help='Source folder')
    parser.add_argument('-d', '--destination', help='Destination folder')
    parser.add_argument('--show-destination', action='store_true',
                        help='Show destination folder (message box)')
    parser.add_argument('--dry-run', action='store_true',
                        help='Don\'t copy, just do the other things')
    parser.add_argument(
        '--print', action='store_true', help='Print current registry settings')
    parser.add_argument(
        '-v', '--verbose', action='count', help='Print status information during work')

    args = parser.parse_args()
    return(args)


def getloggingmode(verbose):
    if verbose:
        verb = verbose
    else:
        verb = 1

    try:
        loggingmode = [
            logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG][verb]
    except IndexError:
        loggingmode = logging.DEBUG
    return loggingmode


def zipdir(source, dest):
    logger = logging.getLogger('zipdir')
    logger.info('Zipping copied files and folders ...')
    print('Zip local folder ...')

    zipf = ZipFile(dest, 'w', ZIP_DEFLATED)

    try:
        for root, dirs, files in walk(source):
            for file in files:
                zipf.write(path.join(root, file), path.relpath(
                    path.join(root, file), path.join(source, '..')))
    except Exception as exc:
        logger.exception(exc)
        print(root, dirs, files)

    zipf.close()
    logger.info('Folder %s successfully zipped to %s', source, dest)


def createlink(name, target):
    logger = logging.getLogger('createlink')
    logger.info('Create a link %s to target %s ...', name, target)
    print('Create link to source ...')

    ok = False
    try:
        shell = Dispatch('WScript.Shell')
        shortcut = shell.CreateShortCut('{}.lnk'.format(name))
        shortcut.Targetpath = target
        shortcut.save()

        logger.info('Link created.')
        ok = True
    except Exception as exc:
        logger.error(exc)
        ok = False

    if not ok:
        # Fall back for a simple text file with the path as the only content
        try:
            file = open(name + '.txt', 'w')
            file.write(target)
            file.close()
            ok = True
        except Exception as exc:
            logger.error(exc)
            ok = False


def get_size(source):
    print('Get size ...')
    return valueunit(foldersize(source), 'B', 1024)


def walkdir(folder):
    '''
    Generator, walks through each file in a directory.

    Got this from https://github.com/tqdm/tqdm/wiki/How-to-make-a-great-Progress-Bar

    :param folder: Base folder
    '''
    for dirpath, _, files in walk(folder):
        for filename in files:
            yield path.abspath(path.join(dirpath, filename))


def recursivecopy(source, destination):
    logger = logging.getLogger('recursivecopy')
    logger.debug(
        'Copying source %s to destination %s ...', source, destination)

    print('Copy {path} ...'.format(path=source))

    counter = 0
    for filepath in walkdir(source):
        counter += 1

    for filepath in tqdm(walkdir(source), total=counter):
        src = filepath
        dst = src.replace(source, destination)
        folder = path.split(dst)[0]
        if not path.isdir(folder):
            makedirs(folder)
        copyfile(src, dst)

    logger.info('Folder %s copied to %s', source, destination)


def set_reg(name, value):
    try:
        winreg.CreateKey(winreg.HKEY_CURRENT_USER, REG_PATH)
        registry_key = winreg.OpenKey(
            winreg.HKEY_CURRENT_USER, REG_PATH, 0, winreg.KEY_WRITE)
        winreg.SetValueEx(registry_key, name, 0, winreg.REG_SZ, value)
        winreg.CloseKey(registry_key)
        return True
    except WindowsError:
        return False


def get_reg(name):
    try:
        registry_key = winreg.OpenKey(
            winreg.HKEY_CURRENT_USER, REG_PATH, 0, winreg.KEY_READ)
        value, _ = winreg.QueryValueEx(registry_key, name)
        winreg.CloseKey(registry_key)
        return value
    except WindowsError:
        return None


def foldersize(folder):
    '''
    Get size of the folder (i.e. sum of size of every file recursively)

    :param folder: Base folder
    '''
    logger = logging.getLogger('foldersize')
    logger.debug('Calculating size ...')
    size = 0
    for root, _, files in walk(folder):
        for file in files:
            try:
                size += path.getsize(path.join(root, file))
            except FileNotFoundError:
                pass
    return size


def valueunit(value, baseunit, factor=1000):
    '''
    Format values with prefix according to its size
    :param value:    Original value
    :param baseunit: Basic physical unit
    :param factor:   Basic factor for thousands (may be set to 1024, for example, in case of a Byte)
    '''
    prefixes = ['', 'k', 'M', 'G', 'T']
    f = 0
    v = value
    while v > factor:
        f += 1
        v /= factor
    try:
        result = (v, prefixes[f] + baseunit)
    except IndexError:
        result = (value, '?')
    return result


def msgbox(message, title=__title__, buttons=MB_OK, icon=MB_ICONINFORMATION):
    '''
    Cover function to display a message box with the given information.

    See documenation of the buttons and icon at
    https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-messagebox

    :param str message: The text to be displayed
    :param str title: Message box title (defaults to the program's title)
    :param int buttons: Buttons to be displayed
    :returns: Button clicked
    :rtype: int
    '''

    return ctypes.windll.user32.MessageBoxW(0, message, title, buttons + icon)


if __name__ == '__main__':
    sys.exit(main())
